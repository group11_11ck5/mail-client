# Required tools:
IDE: Eclipse

GUI: SWT

Required tool: WindowBuilder (https://www.eclipse.org/windowbuilder/download.php)

Using JavaMail API: https://java.net/projects/javamail/pages/Home

# Feature(s) needed to be added:
+ Receive mail with attachment
+ Receive new mails only (old mails save to xml or something)
+ View complicated mail (can see plain text mail for now)
+ Send mail with attachment (can send plain text mail for now)
+ Delete mail (3 modes: client only, server only, client + server)
+ PGP:
  + Generate Public & Private key
  + Import & export key (2 mode: public key ; public + private key)
  + Public key management for frequent contacts
  + Send mail with validation supported
  + Send mail with encryption supported
  + Send mail with encryption & validation supported