import java.io.IOException;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;

@SuppressWarnings("restriction")
public class Util {

	private static final Pattern TAG_REGEX = Pattern.compile("<tab>(.+?)</tab>");
	private static final Pattern TAG_REGEX1 = Pattern.compile("<yuki>(.+?)</yuki>");
	private static String algorithm = "AES";
	private static SecretKeySpec secretKeySpec;

	public static String[] getTagValues(final String str) {
	    final List<String> tagValues = new ArrayList<String>();
	    final Matcher matcher = TAG_REGEX.matcher(str);
	    while (matcher.find()) {
	        tagValues.add(matcher.group(1));
	    }
	    String[] array = tagValues.toArray(new String[tagValues.size()]);
	    return array;
	}	
	
	public static String[] getTagValues1(final String str) {
	    final List<String> tagValues = new ArrayList<String>();
	    final Matcher matcher = TAG_REGEX1.matcher(str);
	    while (matcher.find()) {
	        tagValues.add(matcher.group(1));
	    }
	    String[] array = tagValues.toArray(new String[tagValues.size()]);
	    return array;
	}
	
	public static String SHA1(String input) throws Exception {

		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.update(input.getBytes());
		byte[] output = md.digest();

		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F' };

		StringBuffer buf = new StringBuffer();
		for (int j = 0; j < output.length; j++) {
			buf.append(hexDigit[(output[j] >> 4) & 0x0f]);
			buf.append(hexDigit[output[j] & 0x0f]);
		}
		return buf.toString();
	}
	
	public static String decrypt(String text, PrivateKey key) throws IOException, Exception {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] dec = null;
		dec = decoder.decodeBuffer(text);
		byte[] dectyptedText = null;
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, key);
		dectyptedText = cipher.doFinal(dec);
		return new String(dectyptedText);
	}

	public static String decrypt(String text, PublicKey key) throws IOException, Exception {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] dec = null;
		dec = decoder.decodeBuffer(text);
		byte[] dectyptedText = null;
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, key);
		dectyptedText = cipher.doFinal(dec);
		return new String(dectyptedText);
	}
	
	public static SecretKeySpec setSerectKey(String key) throws IOException {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] get_byte;
		get_byte = decoder.decodeBuffer(key);
		secretKeySpec = new SecretKeySpec(get_byte, "AES");
		return secretKeySpec;
		} 
	
	public static String Keydecrypt(String text, PrivateKey key) throws IOException, Exception {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] dec = null;
		dec = decoder.decodeBuffer(text);
		byte[] dectyptedText = null;
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, key);
		dectyptedText = cipher.doFinal(dec);
		return new String(dectyptedText);
	}
	
	public static String PBdecrypt(String text, PublicKey key) throws IOException, Exception {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] dec = null;
		dec = decoder.decodeBuffer(text);
		byte[] dectyptedText = null;
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, key);
		dectyptedText = cipher.doFinal(dec);
		return new String(dectyptedText);
	}
	
public static String AESdecrypt(String encryptedText,String key) throws Exception {
		Cipher chiper = Cipher.getInstance(algorithm);
		chiper.init(Cipher.DECRYPT_MODE,setSerectKey(key));
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedText);
		byte[] decValue = chiper.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue.replaceAll("(?:\\r\\n|\\n\\r|\\n|\\r)", "");
	}
}
