import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;

import javax.mail.*;

import java.util.Properties;

import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.RowLayout;

public class login {

	protected Shell shell;
	public static Text txtUsername;
	public static Text txtPassword;
	public static String strUsername;
	public static String strPassword;
	public static String strHost;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		login window = new login();
		window.open();
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell(SWT.CLOSE | SWT.MIN | SWT.TITLE);
		shell.setSize(450, 300);
		shell.setText("Login Panel");
		RowLayout rl_shell = new RowLayout(SWT.HORIZONTAL);
		rl_shell.marginHeight = 25;
		rl_shell.justify = true;
		rl_shell.marginTop = 0;
		rl_shell.marginRight = 0;
		rl_shell.marginLeft = 0;
		rl_shell.marginBottom = 0;
		shell.setLayout(rl_shell);

		Group grpLoginForm = new Group(shell, SWT.NONE);
		grpLoginForm.setText("Login Form");
		grpLoginForm.setLayout(new GridLayout(2, false));

		Label lblUsername = new Label(grpLoginForm, SWT.NONE);
		lblUsername.setSize(148, 271);
		lblUsername.setText("Username");

		txtUsername = new Text(grpLoginForm, SWT.BORDER);
		txtUsername.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		txtUsername.setSize(111, 271);
		new Label(grpLoginForm, SWT.NONE);

		Label lblexampleNglekhointgmailcom = new Label(grpLoginForm, SWT.NONE);
		lblexampleNglekhointgmailcom.setSize(88, 271);
		lblexampleNglekhointgmailcom
				.setText("(example: nglekhoi.nt@gmail.com)");

		Label lblPassword = new Label(grpLoginForm, SWT.NONE);
		lblPassword.setSize(74, 271);
		lblPassword.setText("Password");

		txtPassword = new Text(grpLoginForm, SWT.BORDER | SWT.PASSWORD);
		txtPassword.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		txtPassword.setSize(63, 271);
		new Label(grpLoginForm, SWT.NONE);

		Label lblumPassword = new Label(grpLoginForm, SWT.NONE);
		lblumPassword.setText("(um... password .. you know)");

		Label lblServices = new Label(grpLoginForm, SWT.NONE);
		lblServices.setSize(55, 271);
		lblServices.setText("Service");

		final Button btnGmail = new Button(grpLoginForm, SWT.RADIO);
		btnGmail.setSize(49, 271);
		btnGmail.setText("Gmail");
		new Label(grpLoginForm, SWT.NONE);

		final Button btnYahoo = new Button(grpLoginForm, SWT.RADIO);
		btnYahoo.setSize(44, 271);
		btnYahoo.setText("Y!Mail");
		new Label(grpLoginForm, SWT.NONE);

		Label lblChooseYourService = new Label(grpLoginForm, SWT.NONE);
		lblChooseYourService.setSize(40, 271);
		lblChooseYourService
				.setText("Choose your service acording to your username");

		Button btnClear = new Button(grpLoginForm, SWT.NONE);
		btnClear.setSize(34, 271);
		btnClear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent event) {
				txtUsername.setText("");
				txtPassword.setText("");
			}
		});
		btnClear.setText("Clear");
		Button btnLogin = new Button(grpLoginForm, SWT.NONE);
		btnLogin.setSize(37, 271);
		btnLogin.setText("Login");
		btnLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				String Username = txtUsername.getText();
				String Password = txtPassword.getText();
				String url;
				MessageBox messageBox = new MessageBox(shell,
						SWT.ICON_INFORMATION);
				Properties props = new Properties();
				Session session = Session.getInstance(props);
				if (btnGmail.getSelection()) {
					url = "imap.googlemail.com";
					strHost = "gmail";
				} else {
					url = "imap.mail.yahoo.com";
					strHost = "yahoo";
				}
				Store store = null;
				try {
					store = session.getStore("imaps");
					store.connect(url, Username, Password);
				} catch (MessagingException e2) {
					e2.printStackTrace();
					messageBox.setMessage("Error: " + e2.getLocalizedMessage());
					messageBox.open();
				}
				if (store.isConnected()) {
					messageBox.setMessage(Username + " Is Connected");
					messageBox.open();
					strUsername = txtUsername.getText();
					strPassword = txtPassword.getText();

					shell.dispose();
					logon.main(store);
				}
				try {
					store.close();
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
}
