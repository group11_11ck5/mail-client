import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.channels.FileChannel;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableItem;


public class KeyManager extends logon {

	public static void CheckKeyMngExists() {
		String dirName = "keymng";
		File theDir = new File(dirName);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.out.println("creating directory: " + dirName);
			boolean result = theDir.mkdir();
			if (result) {
				System.out.println("DIR created");
			}
		}
	}
	
	public static void CheckUserFolderExists(String name) {
		String dirName = "keymng/" + name;
		File theDir = new File(dirName);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.out.println("creating directory: " + dirName);
			boolean result = theDir.mkdir();
			if (result) {
				System.out.println("DIR created");
			}
		}
	}

	public static void GetExistData(String name) throws IOException {
		String priKeyPath = "keymng/" + name + "/" + name + ".pri";
		File priKey = new File(priKeyPath);
		if (priKey.exists()) {
			txtUserPublicKey.setText(priKeyPath);
		}
		String pubKeyPath = "keymng/" + name + "/" + name + ".pub";
		File pubKey = new File(priKeyPath);
		if (pubKey.exists()) {
			txtUserPrivateKey.setText(pubKeyPath);
		}
		String dirFriendPath = "keymng/" + name + "/friend";
		File dirFriend = new File(dirFriendPath);
		File[] keyFiles = dirFriend.listFiles();
		if (dirFriend.exists()) {
			if (keyFiles.length != 0)
				for (int i = 0; i < keyFiles.length; i++) {
					TableItem item = new TableItem(Keytable, SWT.NONE);
					item.setText(
							0,
							keyFiles[i].getName().substring(0,
									keyFiles[i].getName().length() - 4));
					item.setText(1, keyFiles[i].getPath().replace("\\", "/"));
				}
		}
	}

	public static void CreateKeys(String name) {
		KeyPairGenerator keyGen = null;
		try {
			keyGen = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
		keyGen.initialize(512);
		final KeyPair key = keyGen.generateKeyPair();

		PublicKey pubkey = key.getPublic();
		PrivateKey prikey = key.getPrivate();
		String location = "keymng/" + name;
		String publocation = location + "/" + name + ".pub";
		String prilocation = location + "/" + name + ".pri";
		txtUserPublicKey.setText(publocation);
		txtUserPrivateKey.setText(prilocation);
		File publicKeyFile = new File(publocation);
		File privateKeyFile = new File(prilocation);
		try {
			publicKeyFile.createNewFile();
			privateKeyFile.createNewFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ObjectOutputStream publicKeyOS = null;
		ObjectOutputStream privateKeyOS = null;
		try {
			publicKeyOS = new ObjectOutputStream(new FileOutputStream(
					publicKeyFile));
			privateKeyOS = new ObjectOutputStream(new FileOutputStream(
					privateKeyFile));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			publicKeyOS.writeObject(pubkey);
			privateKeyOS.writeObject(prikey);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			publicKeyOS.close();
			privateKeyOS.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void CreateFriendKey(String name, String friendname,
			File remotekey) throws IOException {
		FileChannel sourceChannel = null;
		FileChannel destChannel = null;
		String dirName = "keymng/" + name + "/friend";
		File theDir = new File(dirName);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.out.println("creating directory: " + dirName);
			boolean result = theDir.mkdir();
			if (result) {
				System.out.println("DIR created");
			}
		}
		String desPath = "keymng/" + name + "/friend/" + friendname + ".pub";
		File dest = new File(desPath);
		try {
			sourceChannel = new FileInputStream(remotekey).getChannel();
			destChannel = new FileOutputStream(dest).getChannel();
			destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
		} finally {
			sourceChannel.close();
			destChannel.close();
		}
		int found = 0;
		for (int i = 0; i < Keytable.getItemCount(); i++) {
			if (Keytable.getItem(i).getText(0).contentEquals(friendname)) {
				found = 1;
				break;
			}
		}
		if (found == 0) {
			TableItem item = new TableItem(Keytable, SWT.NONE);
			item.setText(0, friendname);
			item.setText(1, desPath);
		} else {
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING);
			messageBox.setText("Updated");
			messageBox.setMessage(friendname
					+ " has been updated with new data.");
			messageBox.open();
		}
	}
	
	public KeyManager(Display display) {
		super(display);
		
	}

}
