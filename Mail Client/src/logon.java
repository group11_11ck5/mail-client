import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;

public class logon extends Shell {
	static logon shell;
	protected static Table mailTable;
	private static int lastSelect;
	private Text txtFrom;
	protected static Text txtTo;
	private Text txtCC;
	private Text txtBCC;
	protected static Text txtContent;
	protected static Message[] messages;
	private Text txtSubject;
	private Text txtPath;
	protected static Folder inbox;
	private Table attTable;
	protected static Table Keytable;
	protected static Text txtUserPublicKey;
	protected static Text txtUserPrivateKey;
	private Text txtFriendMail;
	protected static String friendName;
	protected static Text txtPGPMode;
	protected static Text txtSenderHash;
	protected static Text txtReceiverHash;
	protected static Text txtHashStatus;
	protected static Text txtAESKey;
	protected static Text txtRawContent;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(Store store) {
		try {
			Display display = Display.getDefault();
			shell = new logon(display);
			shell.open();
			shell.layout();
			MailFunctions.GetEmailList(shell, store);
			KeyManager.CheckKeyMngExists();
			KeyManager.CheckUserFolderExists(login.strUsername);
			KeyManager.GetExistData(login.strUsername);
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public logon(Display display) {
		super(display, SWT.SHELL_TRIM);
		setMinimumSize(new Point(640, 540));
		setLayout(new FillLayout(SWT.HORIZONTAL));

		SashForm sashForm_1 = new SashForm(this, SWT.NONE);
		TabFolder tabFolder = new TabFolder(sashForm_1, SWT.NONE);

		TabItem tabViewMail = new TabItem(tabFolder, SWT.NONE);
		tabViewMail.setText("View Mails");

		mailTable = new Table(tabFolder, SWT.BORDER | SWT.FULL_SELECTION);
		tabViewMail.setControl(mailTable);
		mailTable.setHeaderVisible(true);
		mailTable.setLinesVisible(true);
		TableColumn column3 = new TableColumn(mailTable, SWT.LEFT);
		column3.setWidth(165);
		column3.setText("Title");
		TableColumn column4 = new TableColumn(mailTable, SWT.NONE);
		column4.setWidth(136);
		column4.setText("From");
		TableColumn column5 = new TableColumn(mailTable, SWT.NONE);
		column5.setWidth(40);
		column5.setText("ID");

		Menu menu = new Menu(mailTable);
		mailTable.setMenu(menu);

		MenuItem itemDeleteMailOnServer = new MenuItem(menu, SWT.NONE);
		itemDeleteMailOnServer.setText("Delete");

		TabItem tabSendMail = new TabItem(tabFolder, SWT.NONE);
		tabSendMail.setText("Send Mail");

		Composite composite = new Composite(tabFolder, SWT.NONE);
		tabSendMail.setControl(composite);
		composite.setLayout(new GridLayout(2, false));

		Label lblFrom = new Label(composite, SWT.NONE);
		lblFrom.setBounds(0, 0, 55, 15);
		lblFrom.setText("From");

		txtFrom = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		txtFrom.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));
		txtFrom.setBounds(0, 0, 76, 21);
		txtFrom.setText(login.strUsername);

		Label lblTo = new Label(composite, SWT.NONE);
		lblTo.setText("To");

		txtTo = new Text(composite, SWT.BORDER);
		txtTo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));

		Label lblCC = new Label(composite, SWT.NONE);
		lblCC.setText("CC");

		txtCC = new Text(composite, SWT.BORDER);
		txtCC.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));

		Label lblBCC = new Label(composite, SWT.NONE);
		lblBCC.setText("BCC");

		txtBCC = new Text(composite, SWT.BORDER);
		txtBCC.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));

		Label lblSubject = new Label(composite, SWT.NONE);
		lblSubject.setText("Subject");

		txtSubject = new Text(composite, SWT.BORDER);
		txtSubject.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblContent = new Label(composite, SWT.NONE);
		lblContent.setText("Content");

		txtContent = new Text(composite, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gd_txtContent = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 2);
		gd_txtContent.heightHint = 184;
		txtContent.setLayoutData(gd_txtContent);
		new Label(composite, SWT.NONE);

		Button btnSelectFile = new Button(composite, SWT.NONE);
		btnSelectFile.setText("Select file(s)");

		txtPath = new Text(composite, SWT.BORDER);
		txtPath.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Button btnSend = new Button(composite, SWT.NONE);
		btnSend.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false,
				1, 1));
		btnSend.setText("Send");
		new Label(composite, SWT.NONE);

		Group group = new Group(composite, SWT.NONE);
		GridData gd_group = new GridData(SWT.FILL, SWT.CENTER, false, false, 2,
				1);
		gd_group.widthHint = 282;
		group.setLayoutData(gd_group);
		group.setText("Choose PGP Mode");

		final Button btnMode1 = new Button(group, SWT.RADIO);
		btnMode1.setText("Mode 1: Authentication Only");
		btnMode1.setBounds(31, 46, 205, 16);

		final Button btnMode2 = new Button(group, SWT.RADIO);
		btnMode2.setText("Mode 2: Confidentiality Only");
		btnMode2.setBounds(31, 68, 205, 16);

		final Button btnMode3 = new Button(group, SWT.RADIO);
		btnMode3.setText("Mode 3: Authentication and Confidentiality");
		btnMode3.setBounds(31, 90, 279, 16);

		Button btnMode0 = new Button(group, SWT.RADIO);
		btnMode0.setText("Mode 0: Normal");
		btnMode0.setSelection(true);
		btnMode0.setBounds(31, 24, 205, 16);

		TabItem tabKeyManage = new TabItem(tabFolder, SWT.NONE);
		tabKeyManage.setText("Key Manager");

		Composite compositeKey = new Composite(tabFolder, SWT.NONE);
		tabKeyManage.setControl(compositeKey);
		FillLayout fl_compositeKey = new FillLayout(SWT.VERTICAL);
		fl_compositeKey.marginWidth = 4;
		fl_compositeKey.marginHeight = 2;
		compositeKey.setLayout(fl_compositeKey);

		Composite composite_3 = new Composite(compositeKey, SWT.NONE);
		composite_3.setLayout(new FormLayout());

		Group grpUsersPublicKey = new Group(composite_3, SWT.NONE);
		FormData fd_grpUsersPublicKey = new FormData();
		fd_grpUsersPublicKey.top = new FormAttachment(0, 10);
		fd_grpUsersPublicKey.left = new FormAttachment(0, 10);
		fd_grpUsersPublicKey.right = new FormAttachment(100, -10);
		grpUsersPublicKey.setLayoutData(fd_grpUsersPublicKey);
		grpUsersPublicKey.setText(login.strUsername + "'s Keys");
		grpUsersPublicKey.setLayout(new FillLayout(SWT.HORIZONTAL));

		Composite composite_2 = new Composite(grpUsersPublicKey, SWT.NONE);
		composite_2.setLayout(new GridLayout(2, false));

		Label lblPublicKey = new Label(composite_2, SWT.NONE);
		lblPublicKey.setText("Public Key");

		txtUserPublicKey = new Text(composite_2, SWT.BORDER | SWT.READ_ONLY);
		txtUserPublicKey.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblPrivateKey = new Label(composite_2, SWT.NONE);
		lblPrivateKey.setText("Private Key");

		txtUserPrivateKey = new Text(composite_2, SWT.BORDER | SWT.READ_ONLY);
		txtUserPrivateKey.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));

		Button btnGenerate = new Button(composite_2, SWT.NONE);
		btnGenerate.setText("Generate");

		Button btnExport = new Button(composite_2, SWT.NONE);
		btnExport.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));

		btnExport.setText("Export Public Key");
		Group grpListOfKeys = new Group(composite_3, SWT.NONE);
		grpListOfKeys.setLayout(new GridLayout(2, false));
		FormData fd_grpListOfKeys = new FormData();
		fd_grpListOfKeys.right = new FormAttachment(grpUsersPublicKey, 0,
				SWT.RIGHT);
		fd_grpListOfKeys.top = new FormAttachment(grpUsersPublicKey, 6);
		fd_grpListOfKeys.left = new FormAttachment(grpUsersPublicKey, 0,
				SWT.LEFT);
		fd_grpListOfKeys.bottom = new FormAttachment(100, -10);
		grpListOfKeys.setLayoutData(fd_grpListOfKeys);
		grpListOfKeys.setText("Friends's Public Key");

		Label lblNewLabel = new Label(grpListOfKeys, SWT.NONE);
		lblNewLabel.setText("Friend mail:");

		txtFriendMail = new Text(grpListOfKeys, SWT.BORDER);
		txtFriendMail.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Button btnImportFriend = new Button(grpListOfKeys, SWT.NONE);
		btnImportFriend.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
				false, false, 2, 1));
		btnImportFriend.setText("Select friend's key and import");

		Keytable = new Table(grpListOfKeys, SWT.FULL_SELECTION);
		Keytable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2,
				1));
		Keytable.setLinesVisible(true);
		Keytable.setHeaderVisible(true);
		TableColumn column = new TableColumn(Keytable, SWT.LEFT);
		column.setWidth(80);
		column.setText("Friend Mail");
		TableColumn column2 = new TableColumn(Keytable, SWT.NONE);
		column2.setWidth(217);
		column2.setText("Key");

		Menu menuKey = new Menu(Keytable);
		Keytable.setMenu(menuKey);

		MenuItem itemExportFriendKey = new MenuItem(menuKey, SWT.NONE);
		itemExportFriendKey.setText("Export");

		MenuItem itemDeleteFriendKey = new MenuItem(menuKey, SWT.NONE);
		itemDeleteFriendKey.setText("Delete");

		Composite composite_1 = new Composite(sashForm_1, SWT.NONE);
		composite_1.setLayout(new FillLayout(SWT.HORIZONTAL));

		TabFolder tabFolder_1 = new TabFolder(composite_1, SWT.NONE);

		TabItem tbtmViewMail = new TabItem(tabFolder_1, SWT.NONE);
		tbtmViewMail.setText("View Mail");

		SashForm sashForm = new SashForm(tabFolder_1, SWT.VERTICAL);
		tbtmViewMail.setControl(sashForm);

		final Browser mailcontent = new Browser(sashForm, SWT.NONE);

		attTable = new Table(sashForm, SWT.BORDER | SWT.FULL_SELECTION);
		attTable.setHeaderVisible(false);
		attTable.setLinesVisible(true);
		sashForm.setWeights(new int[] { 302, 110 });

		TabItem tbtmNewItem = new TabItem(tabFolder_1, SWT.NONE);
		tbtmNewItem.setText("PGP Status");

		Composite composite_5 = new Composite(tabFolder_1, SWT.NONE);
		tbtmNewItem.setControl(composite_5);
		composite_5.setLayout(new GridLayout(2, false));

		Label lblNewLabel_1 = new Label(composite_5, SWT.NONE);
		lblNewLabel_1.setText("PGP Mode");

		txtPGPMode = new Text(composite_5, SWT.BORDER | SWT.READ_ONLY);
		txtPGPMode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_2 = new Label(composite_5, SWT.NONE);
		lblNewLabel_2.setText("Sender Hash");

		txtSenderHash = new Text(composite_5, SWT.BORDER | SWT.READ_ONLY);
		txtSenderHash.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_3 = new Label(composite_5, SWT.NONE);
		lblNewLabel_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_3.setText("Receiver Hash");

		txtReceiverHash = new Text(composite_5, SWT.BORDER | SWT.READ_ONLY);
		txtReceiverHash.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_4 = new Label(composite_5, SWT.NONE);
		lblNewLabel_4.setText("Hash Status");

		txtHashStatus = new Text(composite_5, SWT.BORDER | SWT.READ_ONLY);
		txtHashStatus.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_5 = new Label(composite_5, SWT.NONE);
		lblNewLabel_5.setText("AES Key");

		txtAESKey = new Text(composite_5, SWT.BORDER | SWT.READ_ONLY);
		txtAESKey.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblRawContent = new Label(composite_5, SWT.NONE);
		lblRawContent.setText("Raw Content");
		new Label(composite_5, SWT.NONE);

		txtRawContent = new Text(composite_5, SWT.BORDER | SWT.READ_ONLY
				| SWT.WRAP);
		GridData gd_txtRawContent = new GridData(SWT.FILL, SWT.FILL, true,
				true, 2, 1);
		gd_txtRawContent.widthHint = 146;
		txtRawContent.setLayoutData(gd_txtRawContent);
		sashForm_1.setWeights(new int[] { 313, 316 });

		// Download attachment mouse event
		attTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				Object content = null;
				try {
					content = messages[lastSelect].getContent();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Multipart mp = (Multipart) content;
				String fileName = null;
				try {
					fileName = mp.getBodyPart(
							(int) attTable
									.getItem(attTable.getSelectionIndex())
									.getData()).getFileName();
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
				FileDialog savedialog = new FileDialog(getShell(), SWT.SAVE);
				savedialog.setFilterNames(new String[] { "All Files (*.*)" });
				savedialog.setFilterExtensions(new String[] { "*.*" });
				savedialog.setFilterPath("c:\\"); // Windows path
				savedialog.setFileName(fileName);
				String location = savedialog.open();
				if (location != null)
					{
					MimeBodyPart part = null;
					try {
						part = (MimeBodyPart) mp.getBodyPart((int) attTable
								.getItem(attTable.getSelectionIndex()).getData());
					} catch (MessagingException e1) {
						e1.printStackTrace();
					}
					location = location.replace("\\", "/");
					try {
						part.saveFile(location);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		// Select file mouse event
		btnSelectFile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
				fileDialog.setText("Attachment");
				fileDialog.open();
				fileDialog.getFilterPath();
				txtPath.setText(fileDialog.getFilterPath() + "\\"
						+ fileDialog.getFileName());
			}
		});
		// Read mail mouse event
		mailTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				try {
					lastSelect = Integer.parseInt(mailTable.getItem(mailTable.getSelectionIndex()).getText(2));
					System.out.print(lastSelect+"\n");
					System.out.print(mailTable.getSelectionIndex()+"\n");
					friendName = mailTable.getItem(
							mailTable.getSelectionIndex()).getText(1);
					Object content = messages[lastSelect].getContent();
					if (content instanceof String) {
						String body = (String) content;
						body = PGPDecrypt.ReturnDec(body);
						if (!body.contains("html"))
							body = body.replaceAll("(\r\n|\n\r|\r|\n)",
									"<br />");
						mailcontent.setText(body);
					} else if (content instanceof Multipart) {
						Multipart mp = (Multipart) content;
						String i = null;
						List<Integer> attachL = new ArrayList<>();
						i = MailFunctions.GetMultipartContent(mp);
						attachL = MailFunctions.GetAttachIndex(mp);
						i = PGPDecrypt.ReturnDec(i);
						if (!i.contains("html"))
							i = i.replaceAll("(\r\n|\n\r|\r|\n)", "<br />");
						mailcontent.setText(i);
						attTable.removeAll();
						if (!(attachL == null))
							for (int att : attachL) {
								String fileName = mp.getBodyPart(att)
										.getFileName();
								TableItem item = new TableItem(attTable,
										SWT.NONE);
								item.setData(att);
								item.setText(fileName
										+ " (Double click to download)");
								System.out.print(item.getData() + "\n");
							}
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		// Send mail button mouse event
		btnSend.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				try
				{
					// Mode 1 handle:
					if (btnMode1.getSelection()) {
						txtContent.setText(PGPEncrypt.modeOne_Enc());
					}
					// Mode 2 handle:
					if (btnMode2.getSelection()) {
						txtContent.setText(PGPEncrypt.modeTwo_Enc());
					}
					// Mode 3 handle:
					if (btnMode3.getSelection()) {
						txtContent.setText(PGPEncrypt.modeThree_Enc());
					}
				}
				catch (Exception e1) {
					e1.printStackTrace();
				}

				// Get texts form fields
				String textContent = txtContent.getText();

				try {
					Properties props = new Properties();
					final Session session = Session.getInstance(props);
					final Message msg = new MimeMessage(session);
					// Add Recipients
					Address to = new InternetAddress(txtTo.getText());
					Address from = new InternetAddress(txtFrom.getText());
					Address cc, bcc;
					if (txtCC.getText() != "") {
						cc = new InternetAddress(txtCC.getText());
						msg.setRecipient(Message.RecipientType.CC, cc);
					}
					if (txtBCC.getText() != "") {
						bcc = new InternetAddress(txtBCC.getText());
						msg.setRecipient(Message.RecipientType.BCC, bcc);
					}
					msg.setContent(txtContent.getText(), "text/plain");
					msg.setFrom(from);
					msg.setRecipient(Message.RecipientType.TO, to);
					msg.setSubject(txtSubject.getText());

					// Send attach file
					if (txtPath.getText() != "") {
						MimeBodyPart msgBodyPart = new MimeBodyPart();
						msgBodyPart.setText(textContent);
						Multipart multipart = new MimeMultipart();
						multipart.addBodyPart(msgBodyPart);

						msgBodyPart = new MimeBodyPart();
						javax.activation.DataSource source = new FileDataSource(
								txtPath.getText());
						msgBodyPart.setDataHandler(new DataHandler(source));
						String path = txtPath.getText();
						String filename = path.substring(path.lastIndexOf("\\") + 1);
						msgBodyPart.setFileName(filename);
						multipart.addBodyPart(msgBodyPart);
						msg.setContent(multipart);
					}

					// Account Info
					final String userName = login.strUsername;
					final String password = login.strPassword;
					// SMTP Server Info
					final String hostname = login.strHost;
					final String host;
					if (hostname == "gmail")
						host = "smtp.gmail.com";
					else
						host = "smtp.mail.yahoo.com";

					// Thread handle send mail
					Runnable r = new Runnable() {
						@Override
						public void run() {
							Transport t = null;
							try {
								t = session.getTransport("smtps");
								t.connect(host, userName, password);
								t.sendMessage(msg, msg.getAllRecipients());
							} catch (MessagingException ex) {
								ex.printStackTrace();
							} finally {
								if (t != null)
									try {
										t.close();
									} catch (MessagingException e) {
										// ignore
									}
							}
						}
					};
					Thread t = new Thread(r);
					t.start();
					// clear form
					// txtContent.setText("");
					txtSubject.setText("");
					txtPath.setText("");
					MessageBox messageBox = new MessageBox(getShell(),
							SWT.ICON_INFORMATION);
					messageBox.setMessage("Send mail Successfully !");
					messageBox.open();
				} catch (MessagingException ex) {
					MessageBox messageBox = new MessageBox(getShell(),
							SWT.ICON_INFORMATION);
					messageBox.setMessage("Error:" + ex.getMessage());
					messageBox.open();
				}
			}
		});
		// Delete mail mouse event
		itemDeleteMailOnServer.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				int style = SWT.ICON_WARNING | SWT.YES | SWT.NO;
				MessageBox messageBox = new MessageBox(getShell(), style);
				messageBox.setText("Warning");
				messageBox
						.setMessage("Are you sure you want delete this mail !");
				int choose = messageBox.open();
				int check = 0; // Folder opened
				System.out.println(check);
				switch (choose) {
				case SWT.YES:
					System.out.println("SWT.YES");
					try {
						messages[Integer.parseInt(mailTable.getItem(mailTable.getSelectionIndex()).getText(2))].setFlag(
								Flags.Flag.DELETED, true);
						System.out.println(check);
						System.out.println("Deleted mail");
					} catch (MessagingException e) {
						e.printStackTrace();
					}
					mailTable.remove(mailTable.getSelectionIndices());
					break;
				case SWT.NO:
					System.out.println("SWT.NO");
					break;
				}
			}
		});
		// Generate user key mouse event
		btnGenerate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				KeyManager.CreateKeys(login.strUsername);
			}
		});
		// Import friend key mouse event
		btnImportFriend.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				if (txtFriendMail.getText() == "") {
					MessageBox messageBox = new MessageBox(shell,
							SWT.ICON_WARNING);
					messageBox.setText("Warning");
					messageBox.setMessage("Friend name is null");
					messageBox.open();
				} else {
					FileDialog opendialog = new FileDialog(shell, SWT.OPEN);
					opendialog
							.setFilterNames(new String[] { "All Files (*.*)" });
					opendialog.setFilterExtensions(new String[] { "*.*" });
					opendialog.setFilterPath("c:\\"); // Windows path
					String pubKeyPath = opendialog.open();
					if (pubKeyPath != null) {
						File remotePub = new File(pubKeyPath);
						try {
							KeyManager.CreateFriendKey(login.strUsername,
									txtFriendMail.getText(), remotePub);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
		// Export user public key mouse event
		btnExport.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (txtUserPublicKey.getText() != "") {
					String pubKeyPath = "keymng/" + login.strUsername + "/"
							+ login.strUsername + ".pub";
					FileDialog savedialog = new FileDialog(getShell(), SWT.SAVE);
					savedialog
							.setFilterNames(new String[] { "All Files (*.*)" });
					savedialog.setFilterExtensions(new String[] { "*.*" });
					savedialog.setFilterPath("c:\\"); // Windows path
					savedialog.setFileName(login.strUsername + ".pub");
					String location = savedialog.open();
					if (location != null) {
						FileChannel sourceChannel = null;
						FileChannel destChannel = null;
						File dest = new File(location);
						try {
							sourceChannel = new FileInputStream(pubKeyPath)
									.getChannel();
							destChannel = new FileOutputStream(dest)
									.getChannel();
							destChannel.transferFrom(sourceChannel, 0,
									sourceChannel.size());
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} finally {
							try {
								sourceChannel.close();
								destChannel.close();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}
				} else {
					MessageBox messageBox = new MessageBox(shell,
							SWT.ICON_WARNING);
					messageBox.setText("Warning");
					messageBox.setMessage("User public key not found");
					messageBox.open();
				}
			}

		});
		// Delete friend key mouse event
		itemDeleteFriendKey.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int i = Keytable.getSelectionIndex();
				System.out.print(i + "\n");
				System.out.print(Keytable.getItem(i).getText(0) + "\n");
				System.out.print(Keytable.getItem(i).getText(1) + "\n");
				File delFile = new File(Keytable.getItem(i).getText(1));
				Keytable.remove(i);
				if (delFile.delete()) {
					System.out.print("Friend key deleted");
				}
			}
		});
		// Export friend key mouse event
		itemExportFriendKey.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int i = Keytable.getSelectionIndex();
				String pubKeyPath = Keytable.getItem(i).getText(1);
				String fileName = pubKeyPath.substring(
						pubKeyPath.lastIndexOf("/") + 1, pubKeyPath.length());
				FileDialog savedialog = new FileDialog(getShell(), SWT.SAVE);
				savedialog.setFilterNames(new String[] { "All Files (*.*)" });
				savedialog.setFilterExtensions(new String[] { "*.*" });
				savedialog.setFilterPath("c:\\"); // Windows path
				savedialog.setFileName(fileName);
				String location = savedialog.open();
				if (location != null) {
					FileChannel sourceChannel = null;
					FileChannel destChannel = null;
					File dest = new File(location);
					try {
						sourceChannel = new FileInputStream(pubKeyPath)
								.getChannel();
						destChannel = new FileOutputStream(dest).getChannel();
						destChannel.transferFrom(sourceChannel, 0,
								sourceChannel.size());
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} finally {
						try {
							sourceChannel.close();
							destChannel.close();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
		// MODE 1: Check private key exists or not
		btnMode1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String priKeyPath = "keymng/" + login.strUsername + "/"
						+ login.strUsername + ".pri";
				File f = new File(priKeyPath);
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING);
				// check key exists or not
				if (!f.exists() || f.isDirectory()) {
					btnMode1.setSelection(false);
					messageBox.setText("Warning");
					messageBox
							.setMessage("Your private key is not exists ! \n Please Generate keys");
					messageBox.open();
				}
			}
		});
		// MODE 2: check friend's key and TO field
		btnMode2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String pubKeyPath = "keymng/" + login.strUsername + "/"
						+ "friend/" + txtTo.getText() + ".pub";
				System.out.println(pubKeyPath);
				File f = new File(pubKeyPath);
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING);
				if (txtTo.getText() == "") {
					btnMode2.setSelection(false);
					messageBox.setText("Warning");
					messageBox
							.setMessage("Your TO field is null ! Please fill out");
					messageBox.open();
				} else if (!f.exists() || f.isDirectory()) {
					btnMode2.setSelection(false);
					messageBox.setText("Warning");
					messageBox
							.setMessage("Your friend's public key is not exists ! \n Please select your friend's key");
					messageBox.open();
				}
			}
		});
		// MODE 3: Similar
		btnMode3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String priKeyPath = "keymng/" + login.strUsername + "/"
						+ login.strUsername + ".pri";
				File f = new File(priKeyPath);
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING);
				// check key exists or not
				if (!f.exists() || f.isDirectory()) {
					btnMode3.setSelection(false);
					messageBox.setText("Warning");
					messageBox
							.setMessage("Your private key is not exists ! \n Please Generate keys");
					messageBox.open();
				}

				String pubKeyPath = "keymng/" + login.strUsername + "/"
						+ "friend/" + txtTo.getText() + ".pub";
				File f2 = new File(pubKeyPath);
				if (txtTo.getText() == "") {
					btnMode3.setSelection(false);
					messageBox.setText("Warning");
					messageBox
							.setMessage("Your TO field is null ! Please fill out");
					messageBox.open();
				} else if (!f2.exists() || f2.isDirectory()) {
					btnMode3.setSelection(false);
					messageBox.setText("Warning");
					messageBox
							.setMessage("Your friend's public key is not exists ! \n Please select your friend's key");
					messageBox.open();
				}
			}
		});
		createContents();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("Welcome " + login.strUsername);
		setSize(640, 597);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}