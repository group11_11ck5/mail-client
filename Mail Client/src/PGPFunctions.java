import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.eclipse.swt.widgets.Display;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class PGPFunctions extends logon {
	
	public PGPFunctions(Display display) {
		super(display);
	}

	// Attributes for Mode 2
	private static String algorithm = "AES";
	private static SecretKeySpec secretKeySpec;
	static SecretKey secretKey;
	private static String text_key_AES = "";

	
	/*
	 * Mode 1: Funtions
	 * 
	 */ 
	 
	// Hash Functions
	public static String SHA1(String input) throws Exception {

		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.update(input.getBytes());
		byte[] output = md.digest();

		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F' };

		StringBuffer buf = new StringBuffer();
		for (int j = 0; j < output.length; j++) {
			buf.append(hexDigit[(output[j] >> 4) & 0x0f]);
			buf.append(hexDigit[output[j] & 0x0f]);
		}
		return buf.toString();
	}
	
	// Encrypt Functions
	// -- encrypt private key
	public static String encrypt(String text, PrivateKey key) throws Exception {
		byte[] cipherText = null;
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		cipherText = cipher.doFinal(text.getBytes());
		BASE64Encoder encoder = new BASE64Encoder();
		String pre = encoder.encode(cipherText);
		return  pre.replaceAll("(?:\\r\\n|\\n\\r|\\n|\\r)", "");
		
	}
	// -- encrypt public key
	public static String encrypt(String text, PublicKey key)  throws Exception {
		byte[] cipherText = null;
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		cipherText = cipher.doFinal(text.getBytes());
		BASE64Encoder encoder = new BASE64Encoder();
		String pre = encoder.encode(cipherText);
		return  pre.replaceAll("(?:\\r\\n|\\n\\r|\\n|\\r)", "");
	}
	
	// Load Key
	// -- load public key
	public static PublicKey LoadPB(String name, String friendName) throws ClassNotFoundException, IOException {
		String add = "keymng/" + name + "/" + "friend/"+ friendName + ".pub";
		FileInputStream saveFile = new FileInputStream(add);
		ObjectInputStream save = new ObjectInputStream(saveFile);
		PublicKey pb = null;
		pb = (PublicKey) save.readObject();
		save.close();
		return pb;
	}
	
	// -- load private key
	public static PrivateKey LoadPR(String name) throws ClassNotFoundException, IOException {
		String add = "keymng/" + name + "/" + name + ".pri";
		FileInputStream saveFile = new FileInputStream(add);
		ObjectInputStream save = new ObjectInputStream(saveFile);
		PrivateKey pr;
		pr = (PrivateKey) save.readObject();
		save.close();
		return pr;
	}
	
	/*
	 * Mode 2: Functions
	 * 
	 */
	
	// Create session key (random key)
	public static void generate_key() throws NoSuchAlgorithmException {
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		generator.init(128);
		secretKey = generator.generateKey();
		secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
		text_key_AES = getSecretKey();
	}
	
	public static String getSecretKey() {
		String result = "";
		byte[] byte_array = secretKey.getEncoded();
		BASE64Encoder base64Encoder = new BASE64Encoder();
		result = base64Encoder.encode(byte_array);
		return result;
	}

	public static SecretKeySpec setSerectKey(String key) throws IOException {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] get_byte;
			get_byte = decoder.decodeBuffer(key);
			secretKeySpec = new SecretKeySpec(get_byte, "AES");
			return secretKeySpec;
		} 

	public SecretKeySpec getSecretKeySpec() {
		return secretKeySpec;
	}

	public void setSecretKeySpec(SecretKeySpec secretKeySpec) {
		PGPFunctions.secretKeySpec = secretKeySpec;
	}

	public static String getText_key_AES() {
		return text_key_AES;
	}
	
	// Encrypt Plaintext with session key
	public static String encrypt(String plainText) throws Exception {
		generate_key();
		Cipher chiper = Cipher.getInstance(algorithm);
		chiper.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		byte[] encVal = chiper.doFinal(plainText.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encVal);
		return encryptedValue.replaceAll("(?:\\r\\n|\\n\\r|\\n|\\r)", "");
	}
	
	
}
