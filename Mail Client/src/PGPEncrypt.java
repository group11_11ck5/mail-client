import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.eclipse.swt.widgets.Display;


public class PGPEncrypt extends logon {
	
	public PGPEncrypt(Display display) {
		super(display);
	}

	/*
	 * PGP Mode Encrypt Functions
	 * 
	 */
	
	// Mode 1 Encrypt 
	public static String modeOne_Enc() throws Exception {
		String originalContent = txtContent.getText();
		PrivateKey priKey;
		String result = null;
		priKey = PGPFunctions.LoadPR(login.strUsername);	// Load private key
		String hashContent = PGPFunctions.SHA1(txtContent.getText()); // Hash message content
		String hashContentEnc = PGPFunctions.encrypt(hashContent, priKey); // Encrypt hash message content
		result = "<yuki>1</yuki><yuki>" + "<tab>" + originalContent + "</tab>" + "<tab>" + hashContentEnc + "</tab>" + "</yuki>";
		System.out.print(result);
		return result;
	}
	
	// Mode 2 Encrypt 
	public static String modeTwo_Enc() throws Exception, ClassNotFoundException, IOException {
		String originalContent = txtContent.getText();
		PublicKey pubKey = null;
		String result = null;
		pubKey = PGPFunctions.LoadPB(login.strUsername, txtTo.getText()); // Load friend'spublic key
		String encryptedText = null;
		encryptedText = PGPFunctions.encrypt(originalContent);	// Encrypt message content with session key
		String aesKey = PGPFunctions.getText_key_AES();		// Get string session key
		String encKey = PGPFunctions.encrypt(aesKey, pubKey); // Encrypt session key with receiver's public key
		//txtContent.setText("[hash]" + encKey + "[endhash]"+ encryptedText);
		result = "<yuki>2</yuki><yuki>" + "<tab>" + encKey + "</tab>" + "<tab>" + encryptedText + "</tab>" + "</yuki>";
		System.out.print(result);
		return result;
	}
	
	// Mode 3 Encrypt
	public static String modeThree_Enc() throws Exception, ClassNotFoundException, IOException {
		String encMode1 = modeOne_Enc();
		String result = null;
		PublicKey pubKey = null;
		pubKey = PGPFunctions.LoadPB(login.strUsername, txtTo.getText()); // Load friend'spublic key
		String encryptedText = null;
		encryptedText = PGPFunctions.encrypt(encMode1);	// Encrypt message content of mode 1
		String aesKey = PGPFunctions.getText_key_AES();		// Get string session key
		String encKey = PGPFunctions.encrypt(aesKey, pubKey); // Encrypt session key with receiver's public key
		result = "<yuki>3</yuki><yuki>" + "<tab>" + encKey + "</tab>" + "<tab>" + encryptedText + "</tab>" + "</yuki>";
		System.out.print(result);
		// ----- end: mode 2
		return result;
	}
}
