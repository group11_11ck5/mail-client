import java.io.File;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;


public class PGPDecrypt extends logon {

	public PGPDecrypt(Display display) {
		super(display);
	}
	
	public static int CheckDecRequirement(int mode)
	{
		switch (mode) 
		{
		case 1: // Check friend public key
		{
			if (!new File("keymng/" + login.strUsername + "/" + "friend/"+ friendName + ".pub").exists())
			{
				MessageBox messageBox = new MessageBox(shell,SWT.ICON_WARNING);
				messageBox.setText("Warning");
				messageBox.setMessage("(Mode 1) Friend's public key not found. Display raw data.");
				messageBox.open();
				return 0;
			}
			else
				return 1;
		}
		case 2: // Check user private key
		{
			if (!new File("keymng/" + login.strUsername + "/" + login.strUsername + ".pri").exists())
			{
				MessageBox messageBox = new MessageBox(shell,SWT.ICON_WARNING);
				messageBox.setText("Warning");
				messageBox.setMessage("(Mode 2) User's private key not found. Display raw data.");
				messageBox.open();
				return 0;
			}
			else
				return 1;
		}
		case 3: // Check friend public key & user private key
		{
			if (!new File("keymng/" + login.strUsername + "/" + "friend/"+ friendName + ".pub").exists())
			{
				MessageBox messageBox = new MessageBox(shell,SWT.ICON_WARNING);
				messageBox.setText("Warning");
				messageBox.setMessage("(Mode 3) Friend's public key not found. Display raw data.");
				messageBox.open();
				return 0;
			}
			else
			{
				if (!new File("keymng/" + login.strUsername + "/" + login.strUsername + ".pri").exists())
				{
					MessageBox messageBox = new MessageBox(shell,SWT.ICON_WARNING);
					messageBox.setText("Warning");
					messageBox.setMessage("(Mode 3) User's private key not found. Display raw data.");
					messageBox.open();
					return 0;
				}
				else
					return 1;
			}
		}
		default:
		{
			return 0;
		}
		}
	}
	
	public static String ReturnDec(String text) throws Exception
	{
		txtPGPMode.setText(Integer.toString(0));
		txtSenderHash.setText("");
		txtReceiverHash.setText("");
		txtHashStatus.setText("");
		txtAESKey.setText("");
		String output = text;
		String[] strarray = (Util.getTagValues1(text));
		if (strarray.length == 0)
			return output;
		int foo = 0;
		foo = Integer.parseInt(strarray[0]);
		if (foo == 0)
			return output;
		if (CheckDecRequirement(foo) == 1)
		{
			txtPGPMode.setText(Integer.toString(foo));
			txtRawContent.setText(text);
			switch (foo) 
			{
				case 1:
				{
					output = modeOne_Dec(strarray[1], PGPFunctions.LoadPB(login.strUsername, friendName));
					break;
				}
				case 2:
				{
					output = modeTwo_Dec(strarray[1], PGPFunctions.LoadPR(login.strUsername));
					break;
				}
				case 3:
				{
					output = modeThree_Dec(strarray[1], PGPFunctions.LoadPB(login.strUsername, friendName), PGPFunctions.LoadPR(login.strUsername));
					break;
				}
			}
		}
		else
		{
			return output;
		}
		return output;
	}
	
	public static String modeOne_Dec(String encryp, PublicKey senderKey) throws Exception {
		String[] strarray = (Util.getTagValues(encryp));
		String plainText = strarray[0];
		String decHash = Util.decrypt(strarray[1], senderKey);
		String hashPlain = Util.SHA1(plainText);
		if (hashPlain.equals(decHash))
		{
			txtSenderHash.setText(decHash);
			txtReceiverHash.setText(hashPlain);
			txtHashStatus.setText("Content is valid !");
			return plainText;
		}
		else
		{
			txtSenderHash.setText(decHash);
			txtReceiverHash.setText(hashPlain);
			txtHashStatus.setText("Content is invalid !");
			return plainText;
		}
	}
	
	public static String modeTwo_Dec(String input, PrivateKey reckey) throws IOException, Exception {

		String[] strarray = (Util.getTagValues(input));
		String decKey = Util.Keydecrypt(strarray[0], reckey);
		String plainText = null;
		plainText = Util.AESdecrypt(strarray[1], decKey);
		txtAESKey.setText(decKey);
		return plainText;
	}
	
	public static String modeThree_Dec(String input, PublicKey seKey, PrivateKey recKey) throws IOException, Exception {
		String[] arr1 = (Util.getTagValues(input));
		String decKey = Util.Keydecrypt(arr1[0], recKey);
		String encMess = null;
		encMess = Util.AESdecrypt(arr1[1], decKey);
		String[] arr2 = Util.getTagValues(encMess);
		String plainText = arr2[0];
		String decHash = Util.PBdecrypt(arr2[1], seKey);
		String hashPlain = null;
		hashPlain = Util.SHA1(plainText);
		txtAESKey.setText(decKey);
		if (hashPlain.equals(decHash))
		{
			txtSenderHash.setText(decHash);
			txtReceiverHash.setText(hashPlain);
			txtHashStatus.setText("Content is valid !");
			return plainText;
		}
		else
		{
			txtSenderHash.setText(decHash);
			txtReceiverHash.setText(hashPlain);
			txtHashStatus.setText("Content is invalid !");
			return plainText;
		}
	}
}
