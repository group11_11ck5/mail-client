import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;


public class ImportDialog extends Dialog {

	protected Object result;
	protected Shell ImportDialog;
	private Text txtPrivateKey;
	private Text txtPublicKey;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public ImportDialog(Shell parent, int style) {
		super(parent, SWT.DIALOG_TRIM);
		setText("SWT Dialog");
		open();
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		ImportDialog.open();
		ImportDialog.layout();
		Display display = getParent().getDisplay();
		while (!ImportDialog.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		ImportDialog = new Shell(getParent(), getStyle());
		ImportDialog.setSize(442, 190);
		ImportDialog.setText("Import Dialog");
		
		Group grpImportKeys = new Group(ImportDialog, SWT.NONE);
		grpImportKeys.setText("Import Keys");
		grpImportKeys.setBounds(10, 10, 416, 108);
		
		Label labelPrivateKey = new Label(grpImportKeys, SWT.NONE);
		labelPrivateKey.setText("Private Key");
		labelPrivateKey.setBounds(10, 25, 66, 18);
		
		txtPrivateKey = new Text(grpImportKeys, SWT.BORDER);
		txtPrivateKey.setBounds(80, 22, 260, 21);
		
		Button btnBrowsePrivateKey = new Button(grpImportKeys, SWT.NONE);
		btnBrowsePrivateKey.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fileDialog = new FileDialog(getParent(), SWT.OPEN);
				fileDialog.setText("Private Key Import Dialog");
				fileDialog.setFilterExtensions(new String [] {"*.key", "*.txt", "*.private"});
				fileDialog.open();
				fileDialog.getFilterPath();
				txtPrivateKey.setText(fileDialog.getFilterPath());
			}
		});
		btnBrowsePrivateKey.setText("Browse");
		btnBrowsePrivateKey.setBounds(346, 20, 60, 25);
		
		Label labelPublicKey = new Label(grpImportKeys, SWT.NONE);
		labelPublicKey.setText("Public Key");
		labelPublicKey.setBounds(10, 64, 66, 18);
		
		txtPublicKey = new Text(grpImportKeys, SWT.BORDER);
		txtPublicKey.setBounds(80, 61, 260, 21);
		
		Button btnBrowsePublicKey = new Button(grpImportKeys, SWT.NONE);
		btnBrowsePublicKey.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fileDialog = new FileDialog(getParent(), SWT.OPEN);
				fileDialog.setText("Public Key Import Dialog");
				fileDialog.setFilterExtensions(new String [] {"*.key", "*.txt", "*.public"});
				fileDialog.open();
				fileDialog.getFilterPath();
				txtPublicKey.setText(fileDialog.getFilterPath());
			}
		});
		btnBrowsePublicKey.setText("Browse");
		btnBrowsePublicKey.setBounds(346, 57, 60, 25);
		
		Button btnOK = new Button(ImportDialog, SWT.NONE);
		btnOK.setBounds(122, 127, 75, 25);
		btnOK.setText("OK");
		
		Button btnCancel = new Button(ImportDialog, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ImportDialog.dispose();
			}
		});
		btnCancel.setBounds(229, 127, 75, 25);
		btnCancel.setText("Cancel");

	}
}
