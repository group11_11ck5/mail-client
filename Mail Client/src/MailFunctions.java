import java.util.ArrayList;
import java.util.List;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;


public class MailFunctions extends logon {

	public MailFunctions(Display display) {
		super(display);
	}

	public static void GetEmailList(Shell shell, final Store store) throws Exception {
		Runnable r = new Runnable() {
			@Override
			public void run() {
				// Folder inbox;
				try {
					inbox = store.getFolder("inbox");
					// inbox.open(Folder.READ_ONLY);
					inbox.open(Folder.READ_WRITE);
					messages = inbox.getMessages();
					for (int i = messages.length - 1; i >= 0; i--) {
						final int y = i;
						final String title = messages[y].getSubject();
						final String blank = "(no title)";
						Address[] froms = messages[y].getFrom();
						final String from = froms == null ? null : ((InternetAddress) froms[0]).getAddress();
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								TableItem item = new TableItem(mailTable,
										SWT.NONE);
								if (title != null)
									item.setText(0,title);
								else
									item.setText(0,blank);
								item.setText(1,from);
								item.setText(2,Integer.toString(y));
							}
						});
					}
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}
		};
		Thread t = new Thread(r);
		t.start();
	}

	public static String GetMultipartContent(Multipart mp) throws Exception {
		String textPlain = null;
		String textHTML = null;
		Multipart temp = null;
		for (int i = 0; i <= mp.getCount() - 1; i++) {
			System.out.print(mp.getBodyPart(i).getContentType() + "\n");
			if (mp.getBodyPart(i).getContentType().startsWith("TEXT/PLAIN"))
				textPlain = (String) mp.getBodyPart(i).getContent();
			if (mp.getBodyPart(i).getContentType().startsWith("TEXT/HTML"))
				textHTML = (String) mp.getBodyPart(i).getContent();
			if (mp.getBodyPart(i).getContentType().startsWith("multipart")) {
				temp = (Multipart) mp.getBodyPart(i).getContent();
				textHTML = GetMultipartContent(temp);
			}
		}
		if (textHTML != null)
			return textHTML;
		else
			return textPlain;
	}

	public static List<Integer> GetAttachIndex(Multipart mp) throws Exception {
		List<Integer> attach = new ArrayList<>();
		for (int i = 0; i <= mp.getCount() - 1; i++) {
			System.out.print(mp.getBodyPart(i).getContentType() + "\n");
			if (mp.getBodyPart(i).getContentType().startsWith("APPLICATION")) {
				attach.add(i);
			}
		}
		return attach;
	}
}
